# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [v1.1.0] - 05.08.2016

### Added
- Apache2 license
- First working adminhtml cronjob overivew
- Enable hints by cookie
- Hints on click
- Hints with close button

### Changed

- Add messages to cron item title